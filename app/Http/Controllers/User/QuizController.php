<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Option;
use App\Models\UserAnswer;
use Auth;

class QuizController extends Controller
{
    public function index(){

        $data = Question::with('options')->active()->latest()->paginate(5);
        // return $data;
        return view('user.question',compact('data'));
    
    }

    public function answer(Request $request){

        if(UserAnswer::where(['user_id'=>Auth::id(),'question_id'=>$request->question_id])->exists()){

            return response()->json(['status'=>2 ,'message'=>'You already answered this']);

        }
        else{

            $option = Option::find($request->option_id);

            UserAnswer::create([
                'user_id'=>Auth::id(),
                'question_id'=>$request->question_id ,
                'option_id' => $request->option_id,
                'true'=> $option->true
            ]);

            return response()->json(['status'=> $option->true ,'message'=> $option->true ? 'Option selected is right': 'Option selected is Wrong']);

        }
    }

    public function reset(){

        UserAnswer::where(['user_id'=>Auth::id()])->delete();

        notify()->success('Quiz reset done');
        return redirect()->back();

    }


}
