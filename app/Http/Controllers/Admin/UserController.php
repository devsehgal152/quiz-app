<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Hash , Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = User::user()->paginate(10);
        return view('admin.user.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
    {
        return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|min:2',
            'email' => 'required|email|unique:users,email',
            'confirm_password' => 'required',
            'password' => 'required|min:8|same:confirm_password'
        ]);

        $data = $request->except(['_token', '_method','password']);
        $data['password'] = Hash::make($request->password);
        $data['role_id'] = User::USER;

        User::create($data);
        
        notify()->success('User Added Successfully');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = User::find(base64_decode($id));
        return view('admin.user.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'    => 'required|min:2',
            'email' => 'required|email|unique:users,email,'.$id,
            'confirm_password' => 'nullable',
            'password' => 'nullable|min:8|same:confirm_password'
        ]);

        $user = User::find($id);
        
        $data = $request->except(['_token', '_method','password']);
        
        $data['password'] = Hash::make($request->password);

        $user->update($data);

        notify()->success('User successfuly updated');
        return redirect()->route('users.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::destroy(base64_decode($id));
        
        notify()->success('User deleted successfully');
        return back();
        
    }

}
