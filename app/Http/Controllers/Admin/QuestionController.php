<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Option;
use Hash , Auth;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = Question::paginate(10);
        return view('admin.question.index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
    {
        return view('admin.question.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question'    => 'required|min:5|unique:questions,question',
            'options' => 'required',
            'correct_option' => 'required'
        ]);

        // return $request->all();

        $data = $request->except(['_token', '_method','option']);
        $data['active'] = 1;
        $question = Question::create($data);
        
        foreach($request->options as $key => $val){

            $option['question_id'] = $question->id;
            $option['option'] = $val;
            $option['true'] = $key == $request->correct_option ? '1' : '0';
            Option::create($option);

        }

        notify()->success('Question Added Successfully');
        return redirect()->route('questions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Question::with('options')->find(base64_decode($id));
        // return $data;
        return view('admin.question.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'question'    => 'required|min:5|unique:questions,question,'.$id,
            'options' => 'required',
            'correct_option' => 'required'
        ]);

       
        $data = $request->except(['_token', '_method','option']);

        $question = Question::find($id);
        $question->update($data);

        foreach($request->options as $key=>$val){

            Option::where(['question_id'=> $id, 'id'=> $val['id']])->update([
                'option' => $val['value'],
                'true' => $key == $request->correct_option ? '1' : '0'
            ]);

        }

        notify()->success('Question successfuly updated');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Question::destroy(base64_decode($id));        
        Option::where('question_id',$id)->delete();

        notify()->success('User deleted successfully');
        return back();
        
    }

    public function changeStatus(Request $request)
    {
        
        $question = Question::find($request->id);
        $question->active = $request->status;
        $question->save();
        return $question->active;
    }
}
