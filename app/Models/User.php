<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ADMIN = 1;
    const USER = 2;

    protected $appends = [

        'total_answred',
        'total_correct'

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->role_id == Self::ADMIN;
    }

    public function isUser()
    {
        return $this->role_id == Self::USER;
    }

    public function scopeUser($query)
    {
        return $query->where('role_id',Self::USER);
    }

    public function answers(){

        return $this->hasMany(UserAnswer::class);

    }

    public function getTotalAnsweredAttribute(){

        return $this->answers()->count();

    }

    public function getTotalCorrectAttribute(){

        return $this->answers()->where('true',1)->count();

    }


}
