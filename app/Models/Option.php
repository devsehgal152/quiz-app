<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Option extends Model
{
    use HasFactory;

    protected $appends = ['my_answer','answered'];

    protected $guarded = ['id'];

    public function getAnsweredAttribute(){

        return UserAnswer::where(['option_id' => $this->id,'user_id' => Auth::id()])->exists() ? 1 : 0 ;
        
    }

    
    public function getMyAnswerAttribute(){

        $myanswer = UserAnswer::where(['option_id' => $this->id,'user_id' => Auth::id()])->first();
        
        if($myanswer){
            return $myanswer->true;
        }
        else{
            return 0;
        }
        
    }


}
