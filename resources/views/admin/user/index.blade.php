@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Users</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('users.create')}}" class="btn btn-primary">Add User</a>
        </div>
    </div>
    <div class="row">
        <table class="table table-light table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Created at</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data as $val)
                <tr>
                    <td>{{$val->name}}</td>
                    <td>{{$val->email}}</td>
                    <td>{{$val->created_at}}</td>
                    <td>
                        <a href="{{ route('users.edit', [base64_encode($val->id)]) }}"
                            class="btn btn-primary btn-sm">Edit</a>
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-confirm"
                            data-form="deleteForm-{{ $val->id }}">Delete</a>
                        <form id="deleteForm-{{ $val->id }}"
                            action="{{ route('users.destroy', [base64_encode($val->id)]) }}" method="post">
                            @csrf @method('DELETE')
                        </form>
                    </td>  
                </tr>
                @empty
                <tr>
                    <td colspan="4">No users</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="row">
        {!! $data->links() !!}
    </div>
</div>
@endsection