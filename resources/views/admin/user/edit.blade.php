@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Edit User</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('users.index')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <div class="row">
        <form method="POST" action="{{route('users.update',$data->id)}}">
            @csrf
            @method('PUT')
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Name:</label>
                <input type="text" class="form-control" required id="email" placeholder="Enter Name" value="{{old('name',$data->name)}}" name="name">
            </div>   
            @error('name')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror 
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Email:</label>
                <input type="email" class="form-control" required id="email" placeholder="Enter email" name="email" value="{{old('email',$data->email)}}">
            </div>
            @error('email')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror 
            <div class="mb-3">
                <label for="pwd" class="form-label">Password:</label>
                <input type="password" class="form-control"   id="pwd" placeholder="Enter password" name="password">
            </div>
            @error('password')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <div class="mb-3">
                <label for="pwd" class="form-label">Confirm Password:</label>
                <input type="password" class="form-control"   id="pwd" placeholder="Confirm password" name="confirm_password">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection
