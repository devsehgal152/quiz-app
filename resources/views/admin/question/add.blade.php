@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Add Question</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('questions.index')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <div class="row">
        <form method="POST" action="{{route('questions.store')}}">
            @csrf
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Question:</label>
                <input type="text" class="form-control" required id="email" placeholder="Enter Question" value="{{old('qurestion')}}" name="question">
            </div>   
            @error('question')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror 
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Option 1:</label>
                <input type="text" class="form-control" required id="" placeholder="Enter Option 1" name="options[1]" value="{{old('options.1')}}">
            </div>
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Option 2:</label>
                <input type="text" class="form-control" required id="" placeholder="Enter Option 2" name="options[2]" value="{{old('options.2')}}">
            </div>
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Option 3:</label>
                <input type="text" class="form-control" required id="" placeholder="Enter Option 3" name="options[3]" value="{{old('options.3')}}">
            </div>
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Option 4:</label>
                <input type="text" class="form-control" required id="" placeholder="Enter Option 4" name="options[4]" value="{{old('options.4')}}">
            </div>

            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Correct Option:</label>
                <select class="form-select" required name="correct_option">
                    <option value=''>Select one...</option>  
                    <option value='1' {{old('correct_option') == '1' ? 'selected': ''}}>1</option>
                    <option value='2' {{old('correct_option') == '2' ? 'selected': ''}}>2</option>
                    <option value='3' {{old('correct_option') == '3' ? 'selected': ''}}>3</option>
                    <option value='4' {{old('correct_option') == '4' ? 'selected': ''}}>4</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
