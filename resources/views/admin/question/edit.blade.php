@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Edit Question</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('questions.index')}}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <div class="row">
        <form method="POST" action="{{route('questions.update',$data->id)}}">
            @csrf
            @method('PUT')
            <div class="mb-3 mt-3">
                <label for="email" class="form-label">Question:</label>
                <input type="text" class="form-control" required id="email" placeholder="Enter Question" value="{{old('question',$data->question)}}" name="question">
            </div>   
            @error('question')
                <span class="validation invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror 

            @foreach($data->options as $key => $val)
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Option {{$key+1}}:</label>
                <input type="hidden" name="options[{{$key+1}}][id]" value="{{$val->id}}" >
                <input type="text" class="form-control" required id="" placeholder="Enter Option {{$key+1}}" name="options[{{$key+1}}][value]" value="{{old('options.$key+1.value',$val->option)}}">
            </div>
            @endforeach
            <div class="mb-3 mt-3">
                <label for="option" class="form-label">Correct Option:</label>
                <select class="form-select" required name="correct_option">
                    <option value=''>Select one...</option>  
                    <option value='1' {{old('correct_option') == '1' || $data->correct_option == 1 ? 'selected': ''}}>1</option>
                    <option value='2' {{old('correct_option') == '2' || $data->correct_option == 2 ? 'selected': ''}}>2</option>
                    <option value='3' {{old('correct_option') == '3' || $data->correct_option == 3 ? 'selected': ''}}>3</option>
                    <option value='4' {{old('correct_option') == '4' || $data->correct_option == 4 ? 'selected': ''}}>4</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection