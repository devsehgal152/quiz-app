@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <h5>Questions</h5>
        </div>
        <div class="col-lg-2">
            <a href="{{route('questions.create')}}" class="btn btn-primary">Add Question</a>
        </div>
    </div>
    <div class="row">
        <table class="table table-light table-striped">
            <thead>
            <tr>
                <th>Question</th>
                <th>Active</th>
                <th>Created at</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @forelse($data as $val)
                <tr>
                    <td>{{$val->question}}</td>
                    <td>
                        <input type="button" val-id="{{ $val->id }}" status="{{ $val->active ? 0 : 1 }}"
                            value="{{ $val->active ? 'Active' : 'Inactive' }}"
                            class="changeStatus text-white btn btn-{{ $val->active ? 'success' : 'warning' }} btn-sm">
                    </td>
                    <td>{{$val->created_at}}</td>
                    <td>
                        <a href="{{ route('questions.edit', [base64_encode($val->id)]) }}"
                            class="btn btn-primary btn-sm">Edit</a>
                        <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-confirm"
                            data-form="deleteForm-{{ $val->id }}">Delete</a>
                        <form id="deleteForm-{{ $val->id }}"
                            action="{{ route('questions.destroy', [base64_encode($val->id)]) }}" method="post">
                            @csrf @method('DELETE')
                        </form>
                    </td>  
                </tr>
                @empty
                <tr>
                    <td colspan="4">No Questions</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="row">
        {!! $data->links() !!}
    </div>
</div>
@endsection
@section('extra-scripts')
<script>
    $('.changeStatus').on('click', function(event) {
        event.preventDefault();
        var val_id = $(this).attr('val-id');
        var status = $(this).attr('status');
        var message = "Are you sure you would like to change status of this Question";
        swal({
        title: message,
        icon: 'warning',
        buttons: ["Cancel", "Yes"],
        }).then(function(value) {

            console.log(value);

            if (value == true) {
                $.ajax({
                    url: "{{ route('questions.changeStatus') }}",
                    type: 'get',
                    data: {
                        'id': val_id,
                        'status': status,
                    },
                    success: function(id) {

                        console.log(id);
                        swal({
                            title: 'Success',
                            text: 'This question status has been changed successfully',
                            icon: '',
                        }).then(function() {
                            location.reload();
                        });
                    },
                })
            } else {
             
            }
        });
    });
</script>
@endsection
