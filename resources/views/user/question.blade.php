@extends('layouts.user')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h4> Quiz Questions</h4>
        </div>
        <div class="col-lg-4">
            <h6>Total Answered :- <span class="answered">{{Auth::user()->total_answered}}</span></h6>
            <h6>Total Correct :- <span class="correct">{{Auth::user()->total_correct}}</span></h6>
        </div>
        <div class="col-lg-2">
            <a href="{{route('quiz.reset')}}" class="btn btn-primary">Reset</a>
        </div> 
    </div>
    <div class="row">   
        @foreach($data as $key => $val)
            <h6 class="mt-4"> Question : {{$key + $data->firstItem() }} </h6>
            <div class="card">
                <div class="card-header"><strong>Q : </strong> {{$val->question}}</div>
                <div class="card-body">
                    <form action="">    
                    @foreach($val->options as $keyy => $vall)
                        <div class="form-check mt-2 mb-2">
                            <input type="radio" class="form-check-input answer" id="radio{{$val->id}}{{$vall->id}}" 
                            name="optradio{{$val->id}}" question="{{$val->id}}" value="{{$vall->id}}" {{$vall->answered == 1 ? 'checked':''}}>
                            <label class="form-check-label" for="radio{{$val->id}}{{$vall->id}}">{{$vall->option}}</label>
                        </div>
                    @endforeach
                    </form>
                </div>
                <div class="card-footer">Result :- 
                    @if($val->answered == 1)
                        @if($val->my_answer == 1)
                        <span class="result"><strong style="color:green"> True </strong></span>
                        @else
                        <span class="result"><strong style="color:red"> False </strong></span>
                        @endif
                    @else
                        <span class="result"> No answer</span>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row mt-2">
        {!! $data->links() !!}
    </div>
</div>
@endsection
@section('extra-scripts')
<script>
    var  ajaxRequest = null;

    $('.answer').on('click', function(event) {

        event.preventDefault();

        $.ajaxSetup({cache: false});
        if (ajaxRequest != null) {
            ajaxRequest.abort();
            ajaxRequest = null;
        }   

        var option_id = $(this).val();
        var question_id = $(this).attr('question');
        
         ajaxRequest = $.ajax({
            url: "{{ route('quiz.answer') }}",
            type: 'get',
            context: this,
            data: {
                'option_id': option_id,
                'question_id': question_id,
            },
            success: function(data) {

                if(data.status == 1){
                    toastr.success(data.message)   
                    $(this).prop("checked", true);
                    $(this).parent().parent().parent().next().children('.result').html(' <strong style="color:green"> True </strong>')
                    $('.answered').html(parseInt($('.answered').html())+1);
                    $('.correct').html(parseInt($('.correct').html())+1);
                }
                else if(data.status == 2){
                    toastr.warning(data.message)
                }
                else{
                    toastr.warning(data.message)
                    $(this).prop("checked", true);
                    $(this).parent().parent().parent().next().children('.result').html(' <strong style="color:red"> False </strong>')
                    $('.answered').html(parseInt($('.answered').html())+1);
                }

                ajaxRequest = null
            },
        });
    });
</script>

@endsection