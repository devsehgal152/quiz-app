<!-- A grey horizontal navbar that becomes vertical on small screens -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
  <div class="container justify-content-center">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link {{ request()->segment(1) == 'users' ?'active':''}}" href="{{route('users.index')}}">Manage Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ request()->segment(1) == 'questions' ?'active':''}}" href="{{route('questions.index')}}">Manage Questions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('logout')}}">Logout</a>
      </li>
    </ul>
  </div>
</nav>