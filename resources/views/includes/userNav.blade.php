<!-- A grey horizontal navbar that becomes vertical on small screens -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top mb-5">
  <div class="container">
    <ul class="navbar-nav" style="display: contents">
        <li class="nav-item">
            <a class="nav-link {{ request()->segment(1) == 'user-quiz' ?'active':''}}" href="{{route('quiz.index')}}">Quiz</a>
        </li>
        <li class="nav-item m-2 ">
            <h6 style="color:white">Total Questions:- {{$totalQuestions}} </h6>   
        </li>
        <li class="nav-item ">
            <a class="nav-link" href="{{route('logout')}}">Logout</a>
        </li>
    </ul>
  </div>
</nav>