<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return redirect(route('loginForm'));

})->name('home');

Route::namespace('App\Http\Controllers')->group(function() {


    Route::get('login', 'AuthController@loginform')->name('loginForm');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('logout', 'AuthController@logout')->name('logout');

    Route::middleware(['admin'])->namespace('Admin')->group(function () {

        Route::resource('users', 'UserController');
        Route::get('questions/change-status', 'QuestionController@changeStatus')->name('questions.changeStatus');
        Route::resource('questions', 'QuestionController');

    });
    
    Route::middleware(['user'])->namespace('User')->group(function () {

        Route::get('user-quiz', 'QuizController@index')->name('quiz.index');
        Route::get('quiz-answer', 'QuizController@answer')->name('quiz.answer');
        Route::get('reset-quiz', 'QuizController@reset')->name('quiz.reset');
        
    });

});


